const path = require('path');
const slsw = require('serverless-webpack');

const stage = slsw.lib.webpack.isLocal ? 'dev' : 'prod';
const transpileOnly = stage === 'dev';

module.exports = {
  mode: slsw.lib.webpack.isLocal ? 'development' : 'production',
  entry: slsw.lib.entries,
  devtool: 'source-map',
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, '.webpack'),
    filename: '[name].js',
  },
  target: 'node',
  module: {
    rules: [
      // all files with a `.ts` or `.tsx` extension will be handled by `ts-loader`
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              transpileOnly
            }
          }
        ]
      },
    ],
  },
  externals: ['aws-sdk'],
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      src: path.resolve(__dirname, 'src/'),
      tests: path.resolve(__dirname, 'tests/')
    }
  }
};
