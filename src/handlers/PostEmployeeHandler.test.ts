import PostEmployeeHandler from './PostEmployeeHandler';

jest.mock('src/services/EmployeesService');

describe('PostEmployeeHandler', () => {
  const underTest = new PostEmployeeHandler();

  describe('handle', () => {
    const employeeRequest = { ccasd: 'Any User' };

    beforeEach(() => {
      underTest.employeesService.createNewEmployee = jest.fn().mockResolvedValue({ id: 'any_id', name: employeeRequest.ccasd });
    });

    it('should call employee service with correct params', async () => {
      await underTest.handle({ body: JSON.stringify(employeeRequest)} as any);
      expect(underTest.employeesService.createNewEmployee).toHaveBeenCalledWith(employeeRequest);
    });

    it('should return created employee', async () => {
      const result = await underTest.handle({ body: JSON.stringify(employeeRequest)} as any);
      const employee = JSON.parse(result.body);
      expect(employee).toEqual({ id: 'any_id', name: employeeRequest.ccasd });
    });
  });
});
