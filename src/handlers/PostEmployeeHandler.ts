import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult, Context } from 'aws-lambda';
import EmployeesService from 'src/services/EmployeesService';

export interface EmployeeRequest {
  name: string;
}

export default class PostEmployeeHandler {
  employeesService: EmployeesService;

  constructor() {
    this.employeesService = new EmployeesService();
  }

  async handle(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    const employee: EmployeeRequest = JSON.parse(event.body);
    const createdEmployee = await this.employeesService.createNewEmployee(employee);

    return {
      statusCode: 201,
      body: JSON.stringify(createdEmployee, null, 2)
    }
  }
}

export const postEmployeeHandler: APIGatewayProxyHandler =
  async (event: APIGatewayProxyEvent, _context: Context) => await new PostEmployeeHandler().handle(event);
