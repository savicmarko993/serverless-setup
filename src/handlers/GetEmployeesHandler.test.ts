import GetEmployeesHandler from './GetEmployeesHandler';

jest.mock('src/services/EmployeesService');

describe('GetEmployeesHandler', () => {
  const underTest = new GetEmployeesHandler();

  describe('handle', () => {
    const employees = [
      {
        id: '1',
        name: 'John Doe',
      },
      {
        id: '2',
        name: 'Adam Smith',
      },
      {
        id: '3',
        name: 'Ann Johnson',
      },
      {
        id: '4',
        name: 'Dan Brown',
        position: 'Engineer'
      }
    ];

    beforeEach(() => {
      underTest.employeesService.getAllEmployees = jest.fn().mockResolvedValue(employees);
    });

    it('should call employee service with correct params', async () => {
      await underTest.handle(null);
      expect(underTest.employeesService.getAllEmployees).toHaveBeenCalled();
    });

    it('should return all employees', async () => {
      const result = await underTest.handle(null);
      const employees = JSON.parse(result.body);
      expect(employees).toEqual(employees);
    });
  });
});
