import DeleteEmployeeHandler from './DeleteEmployeeHandler';

jest.mock('src/services/EmployeesService');

describe('DeleteEmployeeHandler', () => {
  const underTest = new DeleteEmployeeHandler();

  describe('handle', () => {
    const employee = {
        id: '1',
        name: 'John Doe',
    };

    describe('when employee for given id exists', function () {
      beforeEach(() => {
        underTest.employeesService.getEmployeeById = jest.fn().mockResolvedValue(employee);
      });

      it('should call employee service with correct params', async () => {
        await underTest.handle({ pathParameters: {id: '1'}} as any);
        expect(underTest.employeesService.deleteEmployee).toHaveBeenCalledWith('1');
      });

      it('should return 200', async () => {
        const result = await underTest.handle({ pathParameters: {id: '1'}} as any);
        expect(result.statusCode).toEqual(200);
      });
    });

    describe('when employee for given id exists', function () {
      beforeEach(() => {
        underTest.employeesService.getEmployeeById = jest.fn().mockResolvedValue(undefined);
      });

      it('should return status code 404', async () => {
        const result = await underTest.handle({ pathParameters: {id: '1'}} as any);
        expect(result.statusCode).toEqual(404);
      });
    });
  });
});
