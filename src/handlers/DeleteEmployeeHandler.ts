import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult, Context } from 'aws-lambda';
import EmployeesService from 'src/services/EmployeesService';

export default class DeleteEmployeeHandler {
  employeesService: EmployeesService;

  constructor() {
    this.employeesService = new EmployeesService();
  }

  async handle(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    const { id } = event.pathParameters;

    const employee = await this.employeesService.getEmployeeById(id);

    if (!employee) {
      return { statusCode: 404, body: null };
    }

    await this.employeesService.deleteEmployee(id);

    return {
      statusCode: 200,
      body: null
    }
  }
}

export const deleteEmployeeHandler: APIGatewayProxyHandler =
  async (event: APIGatewayProxyEvent, _context: Context) => await new DeleteEmployeeHandler().handle(event);
