import GetEmployeeByIdHandler from './GetEmployeeByIdHandler';

jest.mock('src/services/EmployeesService');

describe('GetEmployeeByIdHandler', () => {
  const underTest = new GetEmployeeByIdHandler();

  describe('handle', () => {
    const employee = {
        id: '1',
        name: 'John Doe',
    };

    describe('when employee for given id exists', function () {
      beforeEach(() => {
        underTest.employeesService.getEmployeeById = jest.fn().mockResolvedValue(employee);
      });

      it('should call employee service with correct params', async () => {
        await underTest.handle({ pathParameters: {id: '1'}} as any);
        expect(underTest.employeesService.getEmployeeById).toHaveBeenCalledWith('1');
      });

      it('should return all employees', async () => {
        const result = await underTest.handle({ pathParameters: {id: '1'}} as any);
        const employee = JSON.parse(result.body);
        expect(employee).toEqual(employee);
      });
    });

    describe('when employee for given id exists', function () {
      beforeEach(() => {
        underTest.employeesService.getEmployeeById = jest.fn().mockResolvedValue(undefined);
      });

      it('should return status 404', async () => {
        const result = await underTest.handle({ pathParameters: {id: '1'}} as any);
        expect(result.statusCode).toEqual(404);
      });
    });
  });
});
