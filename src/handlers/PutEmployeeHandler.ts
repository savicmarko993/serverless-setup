import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult, Context } from 'aws-lambda';
import EmployeesService from 'src/services/EmployeesService';
import { Employee } from 'src/types/Employee';

export default class PutEmployeeHandler {
  employeesService: EmployeesService;

  constructor() {
    this.employeesService = new EmployeesService();
  }

  async handle(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    const { id } = event.pathParameters;
    const payload: Employee = JSON.parse(event.body);

    const existingEmployee = await this.employeesService.getEmployeeById(id);
    if (!existingEmployee) {
      return { statusCode: 404, body: null };
    }

    await this.employeesService.updateEmployee(id, payload);

    return {
      statusCode: 204,
      body: null
    }
  }
}

export const putEmployeeHandler: APIGatewayProxyHandler =
  async (event: APIGatewayProxyEvent, _context: Context) => await new PutEmployeeHandler().handle(event);
