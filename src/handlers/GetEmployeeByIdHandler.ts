import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult, Context } from 'aws-lambda';
import EmployeesService from 'src/services/EmployeesService';

export default class GetEmployeeByIdHandler {
  employeesService: EmployeesService;

  constructor() {
    this.employeesService = new EmployeesService();
  }

  async handle(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult>{
    const { id } = event.pathParameters;

    const employee = await this.employeesService.getEmployeeById(id);

    if (!employee) {
      return { statusCode: 404, body: null };
    }

    return {
      statusCode: 200,
      body: JSON.stringify(employee, null, 2)
    }
  }
}

export const getEmployeeByIdHandler: APIGatewayProxyHandler =
  async (event: APIGatewayProxyEvent, _context: Context) => await new GetEmployeeByIdHandler().handle(event);
