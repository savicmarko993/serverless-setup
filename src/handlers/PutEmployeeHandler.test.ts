import PutEmployeeHandler from './PutEmployeeHandler';

jest.mock('src/services/EmployeesService');

describe('PutEmployeeHandler', () => {
  const underTest = new PutEmployeeHandler();

  describe('handle', () => {
    const employee = {
        id: '1',
        name: 'John Doe',
    };

    const event = {
      pathParameters: {id: '1'},
      body: JSON.stringify(employee)
    } as any;

    describe('when employee for given id exists', function () {
      beforeEach(() => {
        underTest.employeesService.getEmployeeById = jest.fn().mockResolvedValue(employee);
      });

      it('should call employee service with correct params', async () => {
        await underTest.handle(event);
        expect(underTest.employeesService.updateEmployee).toHaveBeenCalledWith('1', employee);
      });

      it('should return 204', async () => {
        const result = await underTest.handle(event);
        expect(result.statusCode).toEqual(204);
      });
    });

    describe('when employee for given id exists', function () {
      beforeEach(() => {
        underTest.employeesService.getEmployeeById = jest.fn().mockResolvedValue(undefined);
      });

      it('should return 404', async () => {
        const result = await underTest.handle(event);
        expect(result.statusCode).toEqual(404);
      });
    });
  });
});
