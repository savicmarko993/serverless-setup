import { APIGatewayProxyEvent, APIGatewayProxyHandler, APIGatewayProxyResult, Context } from 'aws-lambda';
import EmployeesService from 'src/services/EmployeesService';

export default class GetEmployeesHandler {
  employeesService: EmployeesService;

  constructor() {
    this.employeesService = new EmployeesService();
  }

  async handle(_event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    const employees = await this.employeesService.getAllEmployees();

    return {
      statusCode: 200,
      body: JSON.stringify(employees, null, 2)
    }
  }
}

export const getEmployeesHandler: APIGatewayProxyHandler =
  async (event: APIGatewayProxyEvent, _context: Context) => await new GetEmployeesHandler().handle(event);
