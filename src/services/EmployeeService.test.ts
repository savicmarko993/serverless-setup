import EmployeesService from './EmployeesService';
import { DbDocument } from '../types/DbDocument';

jest.mock('src/client/DynamoDbClient');

describe('EmployeeService', () => {
  const underTest = new EmployeesService();

  const dbDocuments = [
    {
      PK: 'any_PK',
      SK: 'any_SK',
      payload: {
        id: 'any_id',
        name: 'anyName'
      }
    },
    {
      PK: 'any_PK_2',
      SK: 'any_SK_2',
      payload: {
        id: 'any_id_2',
        name: 'anyName_2'
      }
    }
  ] as DbDocument[];

  describe('getAllEmployees', () => {
    beforeEach(() => {
      underTest.dynamoDbClient.getAllEntriesFor = jest.fn().mockResolvedValue(dbDocuments);
    });

    it('should call db client with correct params', async () => {
      await underTest.getAllEmployees();
      expect(underTest.dynamoDbClient.getAllEntriesFor).toHaveBeenCalledWith('EMPLOYEE');
    });

    it('should map result to domain object properly', async () => {
      const result = await underTest.getAllEmployees();
      expect(result).toEqual([{ id: 'any_id', name: 'anyName' }, { id: 'any_id_2', name: 'anyName_2' }]);
    });
  });

  describe('getEmployeeById', () => {
    beforeEach(() => {
      underTest.dynamoDbClient.getSingleEntry = jest.fn().mockResolvedValue(dbDocuments[0]);
    });

    it('should call db client with correct params', async () => {
      await underTest.getEmployeeById('any_id');
      expect(underTest.dynamoDbClient.getSingleEntry).toHaveBeenCalledWith('EMPLOYEE', 'DETAILS#any_id');
    });

    it('should map result to domain object properly', async () => {
      const result = await underTest.getEmployeeById('any_id');
      expect(result).toEqual({ id: 'any_id', name: 'anyName' });
    });
  });

  describe('createNewEmployee', () => {
    beforeEach(() => {
      underTest.dynamoDbClient.putSingleEntry = jest.fn().mockResolvedValue(dbDocuments[0]);
    });

    it('should call db client with correct params', async () => {
      await underTest.createNewEmployee({ name: 'any_employee_name' });
      expect(underTest.dynamoDbClient.putSingleEntry).toHaveBeenCalledWith(expect.objectContaining(
        {
          PK: 'EMPLOYEE',
          SK: expect.stringContaining('DETAILS'),
          payload: expect.objectContaining({
            name: 'any_employee_name'
          })
        }
      ));
    });

    it('should return created employee', async () => {
      const result = await underTest.createNewEmployee({ name: 'any_employee_name' });
      expect(result).toEqual({ id: 'any_id', name: 'anyName' });
    });
  });

  describe('updateEmployee', () => {
    beforeEach(() => {
      underTest.dynamoDbClient.updateSingleEntry = jest.fn();
    });

    it('should call db client with correct params', async () => {
      await underTest.updateEmployee('any_id', { id:'any_id', name: 'any_employee_name' });
      expect(underTest.dynamoDbClient.updateSingleEntry).toHaveBeenCalledWith(
        {
          PK: 'EMPLOYEE',
          SK: 'DETAILS#any_id',
          payload: {
            id: 'any_id',
            name: 'any_employee_name'
          }
        });
    });
  });

  describe('deleteEmployee', () => {
    beforeEach(() => {
      underTest.dynamoDbClient.deleteSingleEntry = jest.fn();
    });

    it('should call db client with correct params', async () => {
      await underTest.deleteEmployee('any_id');
      expect(underTest.dynamoDbClient.deleteSingleEntry).toHaveBeenCalledWith('EMPLOYEE', 'DETAILS#any_id');
    });
  });
});
