import DynamoDbClient from 'src/client/DynamoDbClient';
import { Employee } from 'src/types/Employee';
import uuid from 'uuid';
import { DbDocument } from 'src/types/DbDocument';
import { EmployeeRequest } from 'src/handlers/PostEmployeeHandler';

const EMPLOYEE_PARTITION_KEY = 'EMPLOYEE';
const DETAILS_PREFIX = 'DETAILS';

export default class EmployeesService {
  dynamoDbClient: DynamoDbClient;

  constructor() {
    this.dynamoDbClient = new DynamoDbClient();
  }

  async getAllEmployees(): Promise<Employee[]> {
    let result = await this.dynamoDbClient.getAllEntriesFor(EMPLOYEE_PARTITION_KEY);
    return result.map(document => document.payload as Employee);
  }

  async getEmployeeById(id: string): Promise<Employee> {
    let result = await this.dynamoDbClient.getSingleEntry(EMPLOYEE_PARTITION_KEY, `${DETAILS_PREFIX}#${id}`);
    return result ? result.payload as Employee : undefined;
  }

  async createNewEmployee(employeeRequest: EmployeeRequest): Promise<Employee> {
    const id = uuid();
    const dbDocument: DbDocument = {
      PK: EMPLOYEE_PARTITION_KEY,
      SK: `${DETAILS_PREFIX}#${id}`,
      payload: {
        id,
        name: employeeRequest.name
      }
    };
    const insertedDbDocument = await this.dynamoDbClient.putSingleEntry(dbDocument);
    return insertedDbDocument.payload as Employee;
  }

  async updateEmployee(id: string, employee: Employee) {
    const dbDocument = {
      PK: EMPLOYEE_PARTITION_KEY,
      SK: `${DETAILS_PREFIX}#${id}`,
      payload: employee
    };
    await this.dynamoDbClient.updateSingleEntry(dbDocument);
  }

  async deleteEmployee(id: string) {
    await this.dynamoDbClient.deleteSingleEntry(EMPLOYEE_PARTITION_KEY, `${DETAILS_PREFIX}#${id}`);
  }
}
