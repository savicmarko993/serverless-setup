import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { DbDocument } from 'src/types/DbDocument';

export default class DynamoDbClient {

  dynamoDbOptions: object;

  constructor() {
    this.dynamoDbOptions = {
      apiVersion: '2012-08-10',
      region: 'eu-central-1',
      convertEmptyValues: true
    }
  }

  async getSingleEntry(PK: string, SK: string): Promise<DbDocument> {
    const { Items } = await new DocumentClient(this.dynamoDbOptions)
      .query({
        TableName: process.env.EMPLOYEES_TABLE_NAME,
        KeyConditionExpression: '#PK = :PK And #SK = :SK',
        ExpressionAttributeValues: {
          ':PK': PK,
          ':SK': SK
        },
        ExpressionAttributeNames: {
          '#PK': 'PK',
          '#SK': 'SK'
        }
      })
      .promise();

    return Items[0] as DbDocument;
  }

  async getAllEntriesFor(PK: string): Promise<DbDocument[]> {
    const { Items } = await new DocumentClient(this.dynamoDbOptions)
      .query({
        TableName: process.env.EMPLOYEES_TABLE_NAME,
        KeyConditionExpression: '#PK = :PK',
        ExpressionAttributeValues: {
          ':PK': PK,
        },
        ExpressionAttributeNames: {
          '#PK': 'PK',
        }
      })
      .promise();

    return Items as DbDocument[];
  }

  async putSingleEntry(dbDocument: DbDocument): Promise<DbDocument> {
     await new DocumentClient(this.dynamoDbOptions)
      .put({
        TableName: process.env.EMPLOYEES_TABLE_NAME,
        Item: dbDocument
      })
      .promise();

     return dbDocument;
  }

  async updateSingleEntry(dbDocument: DbDocument) {
    await new DocumentClient(this.dynamoDbOptions)
      .update({
        TableName: process.env.EMPLOYEES_TABLE_NAME,
        Key: {
          PK: dbDocument.PK,
          SK: dbDocument.SK
        },
        UpdateExpression: 'set payload = :payload',
        ExpressionAttributeValues: {
          ':payload': dbDocument.payload
        }
      })
      .promise();
  }

  async deleteSingleEntry(PK: string, SK: string) {
    await new DocumentClient(this.dynamoDbOptions)
      .delete({
        TableName: process.env.EMPLOYEES_TABLE_NAME,
        Key: {
          "PK": PK,
          "SK": SK
        }
      })
      .promise();
  }
}
