export interface DbDocument {
  PK: string,
  SK: string,
  payload: object
}
