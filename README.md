# Serverless setup project

## About

This is a **simple crud serverless app** which can be used for exercising [serverless](https://www.serverless.com/) or for bootstrapping new projects.

Project contains:
 * [ApiGateway](https://aws.amazon.com/api-gateway/) setup for all CRUD operations with simple domain object with basic request validation
 * Proxy [lambdas](https://aws.amazon.com/lambda/) that handles requests
 * [DynamoDb](https://aws.amazon.com/dynamodb/) integration with [single table design](https://www.alexdebrie.com/posts/dynamodb-single-table/) setup
 * Unit test setup
 * [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)
 * Differentiation between **DEV** and **PROD** deployment (appropriate aws config and credentials need to be set)
 
## Usage

### Install dependencies
 
```shell script
yarn install
```

### Unit tests
 
```shell script
yarn test
```

### Local deployment

For local deployment you will need **$USER** environment variable to be set.
 
```shell script
yarn deploy:local
```

### Dev deployment
 
```shell script
yarn deploy:dev
```

### Prod deployment
 
```shell script
yarn deploy:prod
```
