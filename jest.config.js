module.exports = {
  globals: {
    'ts-jest': {
      diagnostics: false,
      tsConfig: '<rootDir>/tsconfig.json'
    }
  },
  moduleNameMapper: {
    '^src/(.*)$': '<rootDir>/src/$1',
    '^tests/(.*)$': '<rootDir>/tests/$1'
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  testPathIgnorePatterns: ['.*/?dist/?.*'],
  transform: {
    '^.+\\.ts$': 'ts-jest'
  },
  testMatch: [
    '<rootDir>/src/**/?(*.)test.ts',
    '<rootDir>/tests/**/?(*.)(int).test.ts',
    '<rootDir>/tests/**/?(*.)(component).test.ts'
  ],
  testEnvironment: 'node'
};
